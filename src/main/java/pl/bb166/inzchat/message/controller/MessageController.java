package pl.bb166.inzchat.message.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.bb166.inzchat.message.domain.dto.CreateGroupMessageDTO;
import pl.bb166.inzchat.message.domain.dto.CreatePrivateMessageDTO;
import pl.bb166.inzchat.message.domain.dto.MessageDTO;
import pl.bb166.inzchat.message.service.MessageService;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("message")
public class MessageController {

    private MessageService messageService;

    @Autowired
    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("{firstUser}/{secondUser}")
    public List<MessageDTO> getAllMessagesForKeys(@PathVariable String firstUser,
                                                         @PathVariable String secondUser,
                                                         Principal principal) {
        if (principal.getName().equals(firstUser) || principal.getName().equals(secondUser))
            return messageService.getMessagesForFirstAndSecondUser(firstUser, secondUser);
        else
            return Collections.emptyList();
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("{groupName}")
    public List<MessageDTO> getAllMessagesForSingleKey(@PathVariable String groupName) {
        return messageService.getMessagesForGroupName(groupName);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("group")
    public ResponseEntity<Void> createGroupMessage(@RequestBody CreateGroupMessageDTO groupDTO, Principal principal) {
        return messageService.createGroupMessage(groupDTO, principal.getName()) ?
                ResponseEntity.status(HttpStatus.CREATED).build() :
                ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("private")
    public ResponseEntity<Void> createPrivateMessage(@RequestBody CreatePrivateMessageDTO privateDTO, Principal principal) {
        privateDTO.setSecondUser(principal.getName());
        return messageService.createPrivateMessage(privateDTO, principal.getName()) ?
                ResponseEntity.status(HttpStatus.CREATED).build() :
                ResponseEntity.status(HttpStatus.NOT_MODIFIED).build();
    }
}