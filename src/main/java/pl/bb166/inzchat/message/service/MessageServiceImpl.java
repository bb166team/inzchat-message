package pl.bb166.inzchat.message.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.inzchat.message.domain.dto.*;
import pl.bb166.inzchat.message.domain.entity.Group;
import pl.bb166.inzchat.message.domain.entity.GroupMessage;
import pl.bb166.inzchat.message.domain.entity.Private;
import pl.bb166.inzchat.message.domain.entity.PrivateMessage;
import pl.bb166.inzchat.message.domain.mapper.GroupMapper;
import pl.bb166.inzchat.message.domain.mapper.MessageMapper;
import pl.bb166.inzchat.message.domain.mapper.PrivateMapper;
import pl.bb166.inzchat.message.repository.PrivateRepository;
import pl.bb166.inzchat.message.repository.GroupRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class MessageServiceImpl implements MessageService {
    static final Logger log = LoggerFactory.getLogger(MessageServiceImpl.class);

    private PrivateRepository privateRepository;

    private GroupRepository groupRepository;

    private GroupMapper groupMapper;

    private MessageMapper messageMapper;

    private PrivateMapper privateMapper;

    @Autowired
    public MessageServiceImpl(PrivateRepository privateRepository,
                              GroupRepository groupRepository,
                              GroupMapper groupMapper,
                              MessageMapper messageMapper,
                              PrivateMapper privateMapper) {
        this.privateRepository = privateRepository;
        this.groupRepository = groupRepository;
        this.groupMapper = groupMapper;
        this.messageMapper = messageMapper;
        this.privateMapper = privateMapper;
    }

    @Override
    public List<MessageDTO> getMessagesForFirstAndSecondUser(String firstUser, String secondUser) {
        PrivateSortDTO privateSortDTO = new PrivateSortDTO(firstUser, secondUser);
        return privateRepository.findPrivateMessagesByFirstUserAndSecondUser(privateSortDTO.getFirstUser(), privateSortDTO.getSecondUser())
                .map(Private::getMessages)
                .map(Set::stream)
                .orElse(Stream.empty())
                .map(messageMapper::privateMessageToMessageDTO)
                .sorted((a, b) -> a.getDate().compareTo(b.getDate()))
                .collect(Collectors.toList());
    }

    @Override
    public List<MessageDTO> getMessagesForGroupName(String groupName) {
        return groupRepository.findGroupByGroupName(groupName)
                .map(groupMapper::groupToGroupDTO)
                .map(GroupDTO::getMessages)
                .map(List::stream)
                .orElse(Stream.empty())
                .sorted((a, b) -> a.getDate().compareTo(b.getDate()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean createGroupMessage(CreateGroupMessageDTO groupMessageDTO, String author) {
        Optional<Group> groupMessages = groupRepository.findGroupByGroupName(groupMessageDTO.getGroupName());

        if (groupMessages.isPresent()) {
            groupMessages.ifPresent(group -> {
                GroupMessage groupMessage = groupMapper.createGroupMessageDTOToMessage(groupMessageDTO, author);
                group.getMessages().add(groupMessage);
                groupMessage.setGroup(group);
                groupRepository.save(group);
            });
        } else {
            groupRepository.save(groupMapper.createGroupMessageDTOToGroupMessage(groupMessageDTO, author));
        }

        return true;
    }

    @Override
    public boolean createPrivateMessage(CreatePrivateMessageDTO privateMessageDTO, String author) {
        PrivateSortDTO privateSortDTO = new PrivateSortDTO(privateMessageDTO.getFirstUser(), privateMessageDTO.getSecondUser());
        privateMessageDTO.setFirstUser(privateSortDTO.getFirstUser());
        privateMessageDTO.setSecondUser(privateSortDTO.getSecondUser());

        Optional<Private> privateOptional =
                privateRepository.findPrivateMessagesByFirstUserAndSecondUser(
                        privateSortDTO.getFirstUser(),
                        privateSortDTO.getSecondUser()
                );

        if (privateOptional.isPresent()) {
            privateOptional.ifPresent(priv -> {
                PrivateMessage privateMessage = privateMapper.createPrivateMessageDTOTPrivateMessage(privateMessageDTO, author);
                priv.getMessages().add(privateMessage);
                privateMessage.setAPrivate(priv);
                privateRepository.save(priv);
            });
        } else {
            privateRepository.save(privateMapper.createPrivateMessageDTOToPrivate(privateMessageDTO, author));
        }

        return true;
    }
}