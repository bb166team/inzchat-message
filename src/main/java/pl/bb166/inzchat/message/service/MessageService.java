package pl.bb166.inzchat.message.service;

import pl.bb166.inzchat.message.domain.dto.CreateGroupMessageDTO;
import pl.bb166.inzchat.message.domain.dto.CreatePrivateMessageDTO;
import pl.bb166.inzchat.message.domain.dto.MessageDTO;

import java.util.List;


public interface MessageService {
    List<MessageDTO> getMessagesForFirstAndSecondUser(String firstUser, String secondUser);
    List<MessageDTO> getMessagesForGroupName(String groupName);
    boolean createGroupMessage(CreateGroupMessageDTO groupMessageDTO, String author);
    boolean createPrivateMessage(CreatePrivateMessageDTO privateMessageDTO, String author);
}