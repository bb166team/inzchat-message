package pl.bb166.inzchat.message.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.bb166.inzchat.message.domain.entity.Group;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface GroupRepository extends JpaRepository<Group, Long> {
    Optional<Group> findGroupByGroupName(String groupName);
}
