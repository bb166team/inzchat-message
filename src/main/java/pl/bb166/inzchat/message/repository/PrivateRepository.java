package pl.bb166.inzchat.message.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.bb166.inzchat.message.domain.entity.Private;
import pl.bb166.inzchat.message.domain.entity.PrivateMessage;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface PrivateRepository extends JpaRepository<Private, Long> {
    Optional<Private> findPrivateMessagesByFirstUserAndSecondUser(String fUser, String sUser);
}
