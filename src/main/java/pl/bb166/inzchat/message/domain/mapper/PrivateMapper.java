package pl.bb166.inzchat.message.domain.mapper;

import org.mapstruct.*;
import pl.bb166.inzchat.message.domain.dto.CreatePrivateMessageDTO;
import pl.bb166.inzchat.message.domain.entity.Private;
import pl.bb166.inzchat.message.domain.entity.PrivateMessage;

import java.time.LocalDateTime;
import java.util.Collections;


@Mapper(componentModel = "spring", uses = MessageMapper.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PrivateMapper {

    @BeanMapping(qualifiedByName = "createEntity")
    Private createPrivateMessageDTOToPrivate(CreatePrivateMessageDTO createPrivateMessageDTO, String author);

    @BeanMapping(qualifiedByName = "createMessageEntity")
    PrivateMessage createPrivateMessageDTOTPrivateMessage(CreatePrivateMessageDTO createPrivateMessageDTO, String author);

    @Named("createMessageEntity")
    @AfterMapping
    default void createMessageEntity(String author, @MappingTarget PrivateMessage privateMessage) {
        privateMessage.setDate(LocalDateTime.now());
        privateMessage.setAuthor(author);
    }

    @Named("createEntity")
    @AfterMapping
    default void createEntity(CreatePrivateMessageDTO createPrivateMessageDTO, String author, @MappingTarget Private aPrivate) {
        PrivateMessage privateMessage = createPrivateMessageDTOTPrivateMessage(createPrivateMessageDTO, author);

        aPrivate.setMessages(Collections.singleton(privateMessage));
        privateMessage.setAPrivate(aPrivate);
    }
}
