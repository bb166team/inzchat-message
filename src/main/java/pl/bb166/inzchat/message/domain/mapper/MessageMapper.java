package pl.bb166.inzchat.message.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import pl.bb166.inzchat.message.domain.dto.MessageDTO;
import pl.bb166.inzchat.message.domain.entity.GroupMessage;
import pl.bb166.inzchat.message.domain.entity.PrivateMessage;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class MessageMapper {

    protected static DateTimeFormatter FORMATTER =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Mapping(source = "groupMessage.date", qualifiedByName = "date", target = "date")
    public abstract MessageDTO groupMessageToMessageDTO(GroupMessage groupMessage);

    @Mapping(source= "privateMessage.date", qualifiedByName = "date", target = "date")
    public abstract MessageDTO privateMessageToMessageDTO(PrivateMessage privateMessage);

    @Mapping(source = "messageDTO.date", qualifiedByName = "dateR", target = "date")
    public abstract GroupMessage messageDTOToMessage(MessageDTO messageDTO);

    @Named("date")
    public String localDateToString(LocalDateTime localDate) {
        return FORMATTER.format(localDate);
    }

    @Named("dateR")
    public LocalDateTime stringToLocalDate(String date) {
        return LocalDateTime.parse(date, FORMATTER);
    }
}
