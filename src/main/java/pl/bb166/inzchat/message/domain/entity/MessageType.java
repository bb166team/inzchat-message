package pl.bb166.inzchat.message.domain.entity;

public enum MessageType {
    TEXT, FILE
}
