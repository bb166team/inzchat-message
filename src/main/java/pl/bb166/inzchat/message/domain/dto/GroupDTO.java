package pl.bb166.inzchat.message.domain.dto;

import lombok.*;

import java.util.List;

@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GroupDTO {
    @Getter @Setter
    private String groupName;

    @Getter @Setter
    private List<MessageDTO> messages;
}
