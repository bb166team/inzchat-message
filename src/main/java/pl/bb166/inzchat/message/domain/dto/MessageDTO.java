package pl.bb166.inzchat.message.domain.dto;

import lombok.*;
import pl.bb166.inzchat.message.domain.entity.MessageType;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class MessageDTO {
    @Getter
    @Setter
    private String message;

    @Getter @Setter
    private String author;

    @Getter @Setter
    private String date;

    @Getter @Setter
    private MessageType messageType;
}
