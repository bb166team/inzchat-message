package pl.bb166.inzchat.message.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Private {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter @Setter
    private String firstUser;

    @Getter @Setter
    private String secondUser;

    @Getter @Setter
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "aPrivate", fetch = FetchType.EAGER)
    private Set<PrivateMessage> messages;
}
