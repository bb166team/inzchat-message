package pl.bb166.inzchat.message.domain.mapper;

import org.mapstruct.*;
import pl.bb166.inzchat.message.domain.dto.CreateGroupMessageDTO;
import pl.bb166.inzchat.message.domain.dto.GroupDTO;
import pl.bb166.inzchat.message.domain.entity.Group;
import pl.bb166.inzchat.message.domain.entity.GroupMessage;

import java.time.LocalDateTime;
import java.util.Collections;

@Mapper(componentModel = "spring", uses = MessageMapper.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GroupMapper {

    GroupDTO groupToGroupDTO(Group group);

    @BeanMapping(qualifiedByName = "createEntity")
    Group createGroupMessageDTOToGroupMessage(CreateGroupMessageDTO createGroupMessageDTO, String author);

    @BeanMapping(qualifiedByName = "createMessageEntity")
    GroupMessage createGroupMessageDTOToMessage(CreateGroupMessageDTO createGroupMessageDTO, String author);

    @Named("createMessageEntity")
    @AfterMapping
    default void createMessageEntity(String author, @MappingTarget GroupMessage groupMessage) {
        groupMessage.setDate(LocalDateTime.now());
        groupMessage.setAuthor(author);
    }

    @Named("createEntity")
    @AfterMapping
    default void createEntity(CreateGroupMessageDTO createGroupMessageDTO, String author, @MappingTarget Group group) {
        GroupMessage groupMessage = createGroupMessageDTOToMessage(createGroupMessageDTO, author);

        group.setMessages(Collections.singleton(groupMessage));
        groupMessage.setGroup(group);
    }
}
