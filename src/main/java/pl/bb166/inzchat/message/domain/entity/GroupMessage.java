package pl.bb166.inzchat.message.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class GroupMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter @Setter
    private String message;

    @Getter @Setter
    private String author;

    @Enumerated(EnumType.STRING)
    @Getter @Setter
    private MessageType messageType;

    @Getter @Setter
    private LocalDateTime date;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "group_id")
    @Getter @Setter
    private Group group;
}
