package pl.bb166.inzchat.message.domain.dto;

public class PrivateSortDTO {
    private String firstUser;

    private String secondUser;

    public PrivateSortDTO(String firstUser, String secondUser) {
        int res = firstUser.compareTo(secondUser);
        this.firstUser = res < 0 ? firstUser : secondUser;
        this.secondUser = res > 0 ? firstUser : secondUser;
    }

    public String getFirstUser() {
        return firstUser;
    }

    public String getSecondUser() {
        return secondUser;
    }
}
