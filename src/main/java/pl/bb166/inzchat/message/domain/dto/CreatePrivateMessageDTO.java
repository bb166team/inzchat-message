package pl.bb166.inzchat.message.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.bb166.inzchat.message.domain.entity.MessageType;

@AllArgsConstructor
@NoArgsConstructor
public class CreatePrivateMessageDTO {

    @Getter @Setter
    private String firstUser;

    @Getter @Setter
    private String secondUser;

    @Getter @Setter
    private String message;

    @Getter @Setter
    private MessageType messageType;
}
