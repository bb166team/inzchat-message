package pl.bb166.inzchat.message.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PrivateMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter @Setter
    private String message;

    @Getter @Setter
    private String author;

    @Getter @Setter
    @Enumerated(EnumType.STRING)
    private MessageType messageType;

    @Getter @Setter
    private LocalDateTime date;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "private_id")
    @Getter @Setter
    private Private aPrivate;
}
