package pl.bb166.inzchat.message.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.bb166.inzchat.message.domain.dto.MessageDTO;
import pl.bb166.inzchat.message.domain.entity.MessageType;
import pl.bb166.inzchat.message.domain.entity.Private;
import pl.bb166.inzchat.message.domain.entity.PrivateMessage;
import pl.bb166.inzchat.message.domain.mapper.MessageMapper;
import pl.bb166.inzchat.message.domain.mapper.MessageMapperImpl;
import pl.bb166.inzchat.message.repository.PrivateRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class MessageServiceTest {
    protected static DateTimeFormatter FORMATTER =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private MessageMapper messageMapper = new MessageMapperImpl();

    private PrivateRepository privateRepository = mock(PrivateRepository.class);

    @Test
    public void getMessageForFirstAndSecondTest() {
        MessageService messageService = new MessageServiceImpl(
                privateRepository,
                null,
                null,
                messageMapper,
                null
        );

        Private priv = new Private();
        priv.setFirstUser("u1");
        priv.setSecondUser("u2");

        LocalDateTime localDateTime = LocalDateTime.now();

        PrivateMessage messageOne = new PrivateMessage();
        messageOne.setAuthor("us1");
        messageOne.setDate(localDateTime);
        messageOne.setMessage("Test");
        messageOne.setMessageType(MessageType.TEXT);

        priv.setMessages(Collections.singleton(messageOne));

        given(privateRepository.findPrivateMessagesByFirstUserAndSecondUser("u1", "u2"))
                .willReturn(Optional.of(priv));

        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setMessageType(MessageType.TEXT);
        messageDTO.setMessage("Test");
        messageDTO.setDate(FORMATTER.format(localDateTime));
        messageDTO.setAuthor("us1");

        assertThat(messageService.getMessagesForFirstAndSecondUser("u1", "u2"))
                .isEqualTo(new ArrayList(Collections.singleton(messageDTO)));
    }
}
